;; Packages
(require 'cask "~/.cask/cask.el")
(cask-initialize)

(require 'pallet)
(pallet-mode t)

(setq package-enable-at-startup nil)
(package-initialize)

;; Minimalist UI
(load-theme 'darktooth t)
(set-frame-font "Anonymous Pro 14" nil t)

(setq inhibit-startup-message t)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(menu-bar-mode -1)

;; Indent and formatting
(require 'whitespace)
(setq whitespace-style '(face lines-tail))
(setq-default indent-tabs-mode nil)
(add-hook 'before-save-hook
          'delete-trailing-whitespace)

;; Backups
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

;; Company
(require 'company)
(setq company-tooltip-align-annotations t)
(setq company-idle-delay 0.5)
(setq company-minimum-prefix-length 2)

;; Magit settings
(if (eq system-type 'windows-nt)
    (progn (setenv "GIT_ASKPASS" "git-gui--askpass")
           (setenv "SSH_ASKPASS" "git-gui--askpass")
           (setenv "GIT_SSH" "C:\\Users\\cjhowe\\portable\\plink.exe")))

;; Dired
(diredp-toggle-find-file-reuse-dir t)

;; Emacs Lisp
(add-hook 'emacs-lisp-mode-hook
          (lambda ()
            (linum-mode t)
            (whitespace-mode t)
            (company-mode t)
            (eldoc-mode t)
            (paredit-mode t)))

;; Erlang
(require 'erlang-start)
(require 'edts-start)

;; Elixir
(add-hook 'elixir-mode-hook
          (lambda ()
            (linum-mode t)
            (whitespace-mode t)
            (company-mode t)
            (alchemist-mode t)))

;; JavaScript
(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))
(add-to-list 'interpreter-mode-alist '("node" . js2-mode))
(add-hook 'js2-mode-hook
          (lambda ()
            (linum-mode t)
            (whitespace-mode t)
            (flycheck-mode t)
            (setq js2-basic-offset 2)
            (js2-refactor-mode t)
            (js2r-add-keybindings-with-prefix "C-c C-m")))

;; SCSS
(setq css-indent-offset 2)
(require 'scss-mode)
(add-hook 'scss-mode-hook
          (lambda ()
            (linum-mode t)
            (whitespace-mode t)))

;; HTML
(require 'tagedit)
(add-hook 'html-mode-hook
          (lambda ()
            (linum-mode t)
            (whitespace-mode t)
            (tagedit-add-paredit-like-keybindings)
            (tagedit-add-experimental-features)
            (tagedit-mode t)))

;; gpg stuff
(require 'epa-file)
(epa-file-enable)
